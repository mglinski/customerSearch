<?php

namespace App\Http\Controllers;

class CustomerController extends Controller
{
	public function searchForm() {
		return view('search');
	}

	public function searchResults() {
		$query = \Input::get('query');
		$results = \App\Customer::lastName($query)->get();
		return view('search.results', compact('results', 'query'));
	}
}
