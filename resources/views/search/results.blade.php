@extends('search')

@section('content')
    @parent
    <div class="row">
        <div class="col-md-12">
            <h3>Search Results for: "{{ $query }}"</h3>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{ $result->first_name }}</td>
                            <td>{{ $result->last_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection