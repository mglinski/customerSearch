#!/usr/bin/env bash

curl -sS https://getcomposer.org/installer | php

./composer.phar install

cp .env.example .env

./app key:generate
php artisan migrate:install
php artisan migrate
php artisan db:seed
phpunit
