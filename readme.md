# CustomerSearch Test - ![](https://codeship.com/projects/187923/status?branch=master)

Built with Laravel 5.1

## Install

To get started, copy the .env.example file to .env and set the database connection settings.

```bash
cp .env.example .env && nano ./.env
```

Then run the `setup.sh` file to download composer, install all dependencies, generate your database from migrations and fill it with data.

```bash
chmod 0777 setup.sh && ./setup.sh
```

Then run this to start the laravel development webserver:

```bash
php artisan serve --port=8080
```

You can then navigate to http://localhost:8080/ in your web browser and demo the application.

## Tests

You can run `phpunit` on the commandline to run the test suite.
