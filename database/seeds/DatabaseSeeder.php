<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the current Databse
        DB::table('customers')->truncate();

        // Insert 10,000 new names into the customers tables
        $data = [];
        $faker = \Faker\Factory::create();
        for ($i=0; $i< 10000; $i++) {
            $data[] = [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
            ];
        }
        \App\Customer::insert($data);

    }
}
