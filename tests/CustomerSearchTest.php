<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerSearchTest extends TestCase
{
    /**
     * Here we navigate to the customer search page and make sure that we see the correct data on the rendered HTML.
     *
     * @return void
     */
    public function testCustomerSearchIndexExample()
    {
        $this->visit('/')
             ->see('>Search Customers</button>')
             ->see('>Search Customers</h3>');
    }

    /**
     * Here we walk through going to the search page and filling out the search form. We submit the form and then check to make
     * sure the page we arrive at is teh correct URL and has the correct search term in the HTML.
     *
     * @return void
     */
    public function testCustomerSearchResultsExample()
    {
        $this->visit('/')
            ->see('Search Customers')
            ->type('Abbot', 'query')
            ->press('Search Customers')
            ->seePageIs('/search?query=Abbot')
            ->see('Search Results for: "Abbot"');
    }
}
