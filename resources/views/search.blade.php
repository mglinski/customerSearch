@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search Customers</h3>
                </div>
                <div class="panel-body">
                    <form method="GET" action="{{ route('customer_search.search') }}">
                        <div class="form-group">
                            <label for="lastNameInput">Last Name</label>
                            <input type="text" class="form-control" id="lastNameInput" name="query" placeholder="Doe"
                                   value="{{ $query or '' }}" >
                        </div>
                        <button type="submit" class="btn btn-default">Search Customers</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection